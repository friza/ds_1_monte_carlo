package FriZA.DiskretnaSimulacia.generator;

import java.util.Random;

/**
 * Created by Tomino on 03/03/16.
 */
public class SeedGenerator extends BaseGenerator<Long> {

    public SeedGenerator() {
        mRandom = new Random();
    }

    @Override
    public Long generate() {
        return mRandom.nextLong();
    }
}
