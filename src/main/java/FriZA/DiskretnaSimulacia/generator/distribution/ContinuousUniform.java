package FriZA.DiskretnaSimulacia.generator.distribution;

import FriZA.DiskretnaSimulacia.generator.BaseGenerator;

import java.util.Random;

/**
 * Created by Tomino on 02/03/16.
 */
public class ContinuousUniform extends BaseGenerator<Double> {

    private final double mMin;
    private final double mMax;

    public ContinuousUniform(final long seed, final double min, final double max) {
        mMin = min;
        mMax = max;

        mRandom = new Random(seed);
    }

    @Override
    public Double generate() {
        return Math.ceil(mRandom.nextDouble() * (mMax - mMin) + mMin);
    }
}
