package FriZA.DiskretnaSimulacia.generator.distribution;

import FriZA.DiskretnaSimulacia.generator.BaseGenerator;

import java.util.Random;

/**
 * Created by Tomino on 02/03/16.
 */
public class DiscreteUniform extends BaseGenerator<Long> {

    private final double mMin;
    private final double mMax;

    private final double mProbability;

    public DiscreteUniform(final long seed, final double min, final double max) {
        mMin = min;
        mMax = max + 1;
        mProbability = 0;

        mRandom = new Random(seed);
    }

    public DiscreteUniform(final long seed, final double min, final double max, final double probability) {
        mMin = min;
        mMax = max + 1;
        mProbability = probability;

        mRandom = new Random(seed);
    }

    @Override
    public Long generate() {
        return (long) (mRandom.nextInt((int) (mMax - mMin)) + mMin);
    }

    public double getProbability() {
        return mProbability;
    }
}
