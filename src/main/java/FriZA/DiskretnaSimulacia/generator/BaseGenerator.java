package FriZA.DiskretnaSimulacia.generator;

import java.util.Random;

/**
 * Created by Tomino on 02/03/16.
 */
public abstract class BaseGenerator<T> {

    protected Random mRandom;

    public abstract T generate();
}
