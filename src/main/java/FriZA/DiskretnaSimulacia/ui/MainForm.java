package FriZA.DiskretnaSimulacia.ui;

import FriZA.DiskretnaSimulacia.Simulation;
import FriZA.DiskretnaSimulacia.core.IForm;
import FriZA.DiskretnaSimulacia.model.result.FrequencyResult;
import FriZA.DiskretnaSimulacia.model.result.Result;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Tomino on 03/03/16.
 */
public class MainForm implements IForm {

    private JPanel rootPanel;
    private JButton btnStartSim;
    private JLabel lblSimStatus;
    private JLabel lblCountOfReplications;
    private JLabel lblAverageProjectLength;
    private JButton btnStopSimulation;
    private JTextField txtCountOfReplications;
    private JTextField txtCompletedByDays;
    private JLabel lblTerminSuccessResult;
    private JTextArea txtAreaResultsFrequency;
    private JLabel lblCompletedByResult;
    private JTextField txtGraphWarmup;
    private JTabbedPane tabbedPane1;
    private JPanel panelHistogram;
    private JPanel panelProbabilityGraph;
    private JPanel panelDefaultGraph;
    private JTextField txtProbability;

    private ArrayList<FrequencyResult> mResultArray;

    private int mProbability;
    private long mGraphWarmup = 0;
    private boolean mSimulationWasStopped = false;

    private StringBuilder mStringBuilder;
    private Simulation mSimulation;

    private DefaultCategoryDataset mDefaultDataSet;

    public static void main(String[] args) {
        JFrame frame = new JFrame("MainForm");
        frame.setContentPane(new MainForm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setTitle("FRI ZA, Diskretna simulacia - Monte Carlo");
    }

    public MainForm() {
        initUI();
    }

    private void initUI() {
        lblSimStatus.setText("Init...");

        txtAreaResultsFrequency.setLineWrap(true);
        txtAreaResultsFrequency.setWrapStyleWord(true);

        btnStartSim.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startSimulation();
            }
        });
        btnStopSimulation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stopSimulation();
            }
        });
    }

    private void initSimulation(final long countOfReplication) {
        mResultArray = new ArrayList<>();
        mStringBuilder = new StringBuilder();

        mSimulation = new Simulation(this, countOfReplication,
                Integer.parseInt(txtCompletedByDays.getText()));
    }

    private void startSimulation() {
        mProbability = Integer.parseInt(txtProbability.getText());
        mGraphWarmup = Long.parseLong(txtGraphWarmup.getText());
        mDefaultDataSet = new DefaultCategoryDataset();

        initSimulation(Integer.parseInt(txtCountOfReplications.getText()));
        mSimulation.start();

    }

    private void stopSimulation() {
        if (mSimulation != null) {
            mSimulation.stopSimulation();
            mSimulationWasStopped = true;
        }
    }

    @Override
    public void redrawLabels() {
        lblAverageProjectLength.setText(Result.getAverageProjectLength() + " days");
        lblCountOfReplications.setText(Result.getCountOfReplications() + "");

        lblCompletedByResult.setText(Result.getProjectSuccessProbability() + " %");
        lblTerminSuccessResult.setText(Result.getMinimumDaysForProbability(mProbability) + " days");
    }

    @Override
    public void redrawGraphs() {
        if (Result.getCountOfReplications() > mGraphWarmup) {
            mStringBuilder.setLength(0);

            mResultArray = Result.getFrequencyResults();
            for (FrequencyResult result : mResultArray) {
                mStringBuilder.append(result.getProjectLength() + ". days - " + result.getFrequency() + "x\n");
            }

            txtAreaResultsFrequency.setText("");
            txtAreaResultsFrequency.setText(mStringBuilder.toString());

            drawDefaultGraph();
        }
    }

    private void drawDefaultGraph() {
        mDefaultDataSet.addValue(Result.getAverageProjectLengthForGraph(), "Project length in days", Result.getCountOfReplications() + "");
        JFreeChart chart = ChartFactory.createLineChart("Length of project", "Replication", "Project length in days", mDefaultDataSet);

        final CategoryPlot plot = chart.getCategoryPlot();
        plot.getRangeAxis().setAutoRange(true);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);

        panelDefaultGraph.setLayout(new FlowLayout());

        ChartPanel panel = new ChartPanel(chart);
        panelDefaultGraph.removeAll();
        panelDefaultGraph.add(panel);
        panelDefaultGraph.updateUI();
    }

    private void drawProbabilityGraph(final DefaultCategoryDataset dataSet) {
        JFreeChart chart = ChartFactory.createLineChart("Probability of completion", "Project length in days", "Probability of completion", dataSet);

        final CategoryPlot plot = chart.getCategoryPlot();
        plot.getRangeAxis().setAutoRange(true);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setAutoRangeIncludesZero(false);

        panelProbabilityGraph.setLayout(new FlowLayout());

        ChartPanel panel = new ChartPanel(chart);
        panelProbabilityGraph.removeAll();
        panelProbabilityGraph.add(panel);
        panelProbabilityGraph.updateUI();
    }

    private void drawHistogram(final XYSeries series) {
        XYSeriesCollection dataSet = new XYSeriesCollection(series);
        JFreeChart chart = ChartFactory.createXYBarChart("Length of project", "Project length in days", false, "Frequency", dataSet, PlotOrientation.VERTICAL, false, false, false);

        XYPlot plot = chart.getXYPlot();
        plot.setRangeGridlinePaint(Color.BLACK);

        panelHistogram.setLayout(new FlowLayout());

        ChartPanel panel = new ChartPanel(chart);
        panelHistogram.removeAll();
        panelHistogram.add(panel);
        panelHistogram.updateUI();
    }

    @Override
    public void simulationStarted() {
        lblSimStatus.setText("Working");
    }

    @Override
    public void simulationFinished() {
        if (mSimulationWasStopped) {
            lblSimStatus.setText("Stopped");
        } else {
            lblSimStatus.setText("Finished");
        }


        final XYSeries series = new XYSeries("Random Data");
        DefaultCategoryDataset probabilityDataSet = new DefaultCategoryDataset();

        mResultArray = Result.getFrequencyResults();
        for (FrequencyResult result : mResultArray) {
            mStringBuilder.append(result.getProjectLength() + ". days - " + result.getFrequency() + "x\n");

            series.add(result.getProjectLength(), result.getFrequency());
            probabilityDataSet.addValue(Result.getProbability(result.getProjectLength()), "Probability of completion", result.getProjectLength() + "");
        }

        drawHistogram(series);
        drawProbabilityGraph(probabilityDataSet);
    }
}
