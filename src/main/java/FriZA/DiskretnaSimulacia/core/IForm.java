package FriZA.DiskretnaSimulacia.core;

/**
 * Created by Tomino on 02/03/16.
 */
public interface IForm {

    void redrawLabels();
    void redrawGraphs();
    void simulationStarted();
    void simulationFinished();
}
