package FriZA.DiskretnaSimulacia.core;

import FriZA.DiskretnaSimulacia.generator.SeedGenerator;

/**
 * Created by Tomino on 02/03/16.
 */
public abstract class MonteCarlo extends Thread {

    private final static int TEXT_MOD = 1000;
    private final static int GRAPH_MOD = 100000;

    private final IForm mForm;
    protected final SeedGenerator mSeedGenerator;

    private final long mCountOfReplication;
    private boolean mSimulationWasStopped = false;

    public MonteCarlo(final IForm form, final long countOfReplication) {
        mForm = form;
        mCountOfReplication = countOfReplication;

        mSeedGenerator = new SeedGenerator();
    }

    protected abstract void doReplication(final int numberOfReplication);

    public void stopSimulation() {
        mSimulationWasStopped = true;
    }

    @Override
    public void run() {
        mForm.simulationStarted();
        for (int i = 1; i <= mCountOfReplication; i++) {
            doReplication(i);

            drawResults(i);
            if (mSimulationWasStopped) {
                mForm.simulationFinished();
                return;
            }
        }

        mForm.simulationFinished();
    }

    private void drawResults(final int replicationNumber) {
        if (replicationNumber % TEXT_MOD == 0) {
            mForm.redrawLabels();
            if (replicationNumber % GRAPH_MOD == 0) {
                mForm.redrawGraphs();
            }
        }
    }
}
