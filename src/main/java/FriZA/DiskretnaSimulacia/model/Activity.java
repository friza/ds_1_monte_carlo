package FriZA.DiskretnaSimulacia.model;

import FriZA.DiskretnaSimulacia.generator.distribution.ContinuousUniform;
import FriZA.DiskretnaSimulacia.generator.distribution.DiscreteEmpirical;
import FriZA.DiskretnaSimulacia.generator.distribution.DiscreteUniform;

/**
 * Created by Tomino on 02/03/16.
 */
public class Activity {

    private final String mName;
    private double mLength;

    private ContinuousUniform mContinuousUniformGenerator;
    private DiscreteEmpirical mDiscreteEmpiricalGenerator;
    private DiscreteUniform mDiscreteUniformGenerator;

    public Activity(final String name, final ContinuousUniform continuousUniformGenerator) {
        mContinuousUniformGenerator = continuousUniformGenerator;
        mName = name;
    }

    public Activity(final String name, final DiscreteEmpirical discreteEmpiricalGenerator) {
        mDiscreteEmpiricalGenerator = discreteEmpiricalGenerator;
        mName = name;
    }

    public Activity(final String name, final DiscreteUniform discreteUniformGenerator) {
        mDiscreteUniformGenerator = discreteUniformGenerator;
        mName = name;
    }

    public void generateLength() {
        if (mContinuousUniformGenerator != null) {
            mLength = mContinuousUniformGenerator.generate().longValue();
        } else if (mDiscreteEmpiricalGenerator != null) {
            mLength = mDiscreteEmpiricalGenerator.generate();
        } else if (mDiscreteUniformGenerator != null) {
            mLength = mDiscreteUniformGenerator.generate();
        }
    }

    public String getName() {
        return mName;
    }

    public double getLength() {
        return mLength;
    }

    public void setLength(final double length) {
        mLength = length;
    }
}
