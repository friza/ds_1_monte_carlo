package FriZA.DiskretnaSimulacia.model.result;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Tomino on 02/03/16.
 */
public class Result {

    private static boolean mWasIncremented;

    private static double mLengthOfProject;
    private static long mCountOfReplications;
    private static long mCountOfSuccessfulReplications;

    private static ArrayList<FrequencyResult> mFrequencyArray;

    public static void reset() {
        mLengthOfProject = 0;
        mCountOfReplications = 0;
        mCountOfSuccessfulReplications = 0;

        mFrequencyArray = new ArrayList<>();
    }

    public static void incrementCountOfSuccessfulReplications() {
        mCountOfSuccessfulReplications++;
    }

    public static void addLengthOfProject(final double projectLength) {
        mLengthOfProject += projectLength;
    }

    public static void setCountOfReplications(long mCountOfReplications) {
        Result.mCountOfReplications = mCountOfReplications;
    }

    private static String getDecimalFormat(final double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value).toString();
    }

    /**
     * get average project length in days
     *
     * @return number of days in decimal variant
     */
    public static String getAverageProjectLength() {
        return mLengthOfProject / getCountOfReplications() + "";
    }

    public static double getAverageProjectLengthForGraph() {
        return (mLengthOfProject / getCountOfReplications());
    }

    public static double getProjectSuccessProbability() {
        return ((double) mCountOfSuccessfulReplications / getCountOfReplications()) * 100;
    }

    public static long getCountOfReplications() {
        return mCountOfReplications;
    }

    public static void registerFrequencyResult(final FrequencyResult frequencyResult) {
        mWasIncremented = false;
        for (FrequencyResult result : mFrequencyArray) {
            if (result.equals(frequencyResult)) {
                result.incrementFrequency();

                mWasIncremented = true;
                break;
            }
        }

        if (!mWasIncremented) {
            mFrequencyArray.add(frequencyResult);
        }
    }

    public static ArrayList<FrequencyResult> getFrequencyResults() {
        if (mFrequencyArray == null) {
            return new ArrayList<>();
        }

        Collections.sort(mFrequencyArray, new Comparator<FrequencyResult>() {
            @Override
            public int compare(FrequencyResult o1, FrequencyResult o2) {
                return (int) (o1.getProjectLength() - o2.getProjectLength());
            }
        });

        return mFrequencyArray;
    }

    public static double getMinimumDaysForProbability(final int probabilityLimit) {
        double compareValue;
        double length = 0;
        int frequency = 0;

        for (FrequencyResult frequencyResult : mFrequencyArray) {
            length = frequencyResult.getProjectLength();
            frequency += frequencyResult.getFrequency();

            compareValue = ((double) frequency / getCountOfReplications()) * 100;
            if (compareValue >= probabilityLimit) {
                break;
            }
        }

        return length;
    }

    public static double getProbability(final double length) {
        double result = 0;
        for (FrequencyResult frequencyResult : mFrequencyArray) {
            if (frequencyResult.getProjectLength() <= length) {
                result += ((double) frequencyResult.getFrequency() / getCountOfReplications()) * 100;
            }
        }

        return result;
    }
}
