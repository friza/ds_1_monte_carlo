package FriZA.DiskretnaSimulacia.model.result;

/**
 * Created by Tomino on 04/03/16.
 */
public class FrequencyResult {

    private double mProjectLength;
    private long mFrequency;

    public FrequencyResult(final double projectLength) {
        mProjectLength = projectLength;
        mFrequency = 1;
    }

    public double getProjectLength() {
        return mProjectLength;
    }

    public void setProjectLength(long mProjectLength) {
        this.mProjectLength = mProjectLength;
    }

    public long getFrequency() {
        return mFrequency;
    }

    public void incrementFrequency() {
        this.mFrequency++;
    }

    @Override
    public boolean equals(Object obj) {
        boolean retVal = false;

        if (obj instanceof FrequencyResult){
            FrequencyResult result = (FrequencyResult) obj;
            retVal = result.mProjectLength == this.mProjectLength;
        }

        return retVal;
    }
}
