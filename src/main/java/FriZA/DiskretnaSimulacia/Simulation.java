package FriZA.DiskretnaSimulacia;

import FriZA.DiskretnaSimulacia.core.IForm;
import FriZA.DiskretnaSimulacia.core.MonteCarlo;
import FriZA.DiskretnaSimulacia.generator.PercentGenerator;
import FriZA.DiskretnaSimulacia.generator.distribution.ContinuousUniform;
import FriZA.DiskretnaSimulacia.generator.distribution.DiscreteEmpirical;
import FriZA.DiskretnaSimulacia.generator.distribution.DiscreteUniform;
import FriZA.DiskretnaSimulacia.generator.object.DiscreteUniformConstruct;
import FriZA.DiskretnaSimulacia.model.Activity;
import FriZA.DiskretnaSimulacia.model.result.FrequencyResult;
import FriZA.DiskretnaSimulacia.model.result.Result;

import java.util.Arrays;

/**
 * Created by Tomino on 03/03/16.
 */
public class Simulation extends MonteCarlo {

    private final static double SEVENTH_ACTIVITY_EXTENSION_OF = 1.15;
    private final static double PROBABILITY_OF_REJECTION_FOURTH_ACTIVITY = 0.32;

    private double mProjectLength;
    private long mMainWayOne;
    private long mMainWayTwo;
    private long mSubWayThree;
    private long mSubWayFour;

    private PercentGenerator mPercentGenerator;
    private Activity mActivity1;
    private Activity mActivity2;
    private Activity mActivity3;
    private Activity mActivity4;
    private Activity mActivity5;
    private Activity mActivity6;
    private Activity mActivity7;
    private Activity mActivity8;
    private Activity mActivity9;

    private final int mDaysToComplete;

    public Simulation(final IForm form, final long countOfReplication, final int daysToComplete) {
        super(form, countOfReplication);
        mDaysToComplete = daysToComplete;

        initSimulation();
        Result.reset();
    }

    @Override
    protected void doReplication(final int numberOfReplication) {
        clearValues();

        generateLengthOfActivities();
        if (fourthActivityWillBeRejected()) {
            extendSeventhActivity();

            mProjectLength += mActivity1.getLength();
            mProjectLength += mActivity9.getLength();

            mMainWayTwo += mActivity3.getLength();
            mMainWayTwo += mActivity7.getLength();
            mMainWayTwo += mActivity8.getLength();

            mMainWayOne += mActivity2.getLength();
            mMainWayOne += mActivity6.getLength();
            mMainWayOne += mActivity5.getLength();

            if (mMainWayOne < mMainWayTwo) {
                mProjectLength += mMainWayTwo;
            } else {
                mProjectLength += mMainWayOne;
            }
        } else {
            mProjectLength += mActivity1.getLength();
            mProjectLength += mActivity9.getLength();

            mMainWayTwo += mActivity3.getLength();
            mMainWayTwo += mActivity7.getLength();
            mMainWayTwo += mActivity8.getLength();

            mSubWayThree += mActivity2.getLength();
            mSubWayThree += mActivity6.getLength();

            mSubWayFour += mActivity3.getLength();
            mSubWayFour += mActivity4.getLength();

            if (mSubWayThree < mSubWayFour) {
                mMainWayOne += mSubWayFour;
            } else {
                mMainWayOne += mSubWayThree;
            }

            mMainWayOne += mActivity5.getLength();

            if (mMainWayOne < mMainWayTwo) {
                mProjectLength += mMainWayTwo;
            } else {
                mProjectLength += mMainWayOne;
            }
        }

        saveResults(numberOfReplication);
    }

    private void saveResults(final int numberOfReplication) {
        Result.setCountOfReplications(numberOfReplication);
        Result.addLengthOfProject(mProjectLength);
        Result.registerFrequencyResult(new FrequencyResult(mProjectLength));

        if (mProjectLength <= mDaysToComplete) {
            Result.incrementCountOfSuccessfulReplications();
        }
    }

    private void extendSeventhActivity() {
        mActivity7.setLength(mActivity7.getLength() * SEVENTH_ACTIVITY_EXTENSION_OF);
    }

    private boolean fourthActivityWillBeRejected() {
        return mPercentGenerator.generate() < PROBABILITY_OF_REJECTION_FOURTH_ACTIVITY;
    }

    private void clearValues() {
        mProjectLength = 0;
        mMainWayOne = 0;
        mMainWayTwo = 0;
        mSubWayThree = 0;
        mSubWayFour = 0;
    }

    private void generateLengthOfActivities() {
        mActivity1.generateLength();
        mActivity2.generateLength();
        mActivity3.generateLength();
        mActivity4.generateLength();
        mActivity5.generateLength();
        mActivity6.generateLength();
        mActivity7.generateLength();
        mActivity8.generateLength();
        mActivity9.generateLength();
    }

    private void initSimulation() {
        mPercentGenerator = new PercentGenerator(mSeedGenerator.generate());

        mActivity1 = new Activity("Activity n.1",
                new DiscreteUniform(mSeedGenerator.generate(), 4, 15));
        mActivity2 = new Activity("Activity n.2",
                new DiscreteEmpirical(mSeedGenerator, Arrays.asList(
                        new DiscreteUniformConstruct(10, 29, 0.2),
                        new DiscreteUniformConstruct(30, 48, 0.4),
                        new DiscreteUniformConstruct(49, 65, 0.4)
                )));
        mActivity3 = new Activity("Activity n.3",
                new DiscreteUniform(mSeedGenerator.generate(), 48, 92));
        mActivity4 = new Activity("Activity n.4",
                new DiscreteEmpirical(mSeedGenerator, Arrays.asList(
                        new DiscreteUniformConstruct(19, 27, 0.2),
                        new DiscreteUniformConstruct(28, 44, 0.8)
                )));
        mActivity5 = new Activity("Activity n.5",
                new DiscreteEmpirical(mSeedGenerator, Arrays.asList(
                        new DiscreteUniformConstruct(5, 19, 0.2),
                        new DiscreteUniformConstruct(20, 39, 0.5),
                        new DiscreteUniformConstruct(40, 55, 0.3)
                )));
        mActivity6 = new Activity("Activity n.6",
                new ContinuousUniform(mSeedGenerator.generate(), 10, 16));
        mActivity7 = new Activity("Activity n.7",
                new ContinuousUniform(mSeedGenerator.generate(), 20, 29));
        mActivity8 = new Activity("Activity n.8",
                new ContinuousUniform(mSeedGenerator.generate(), 12, 17));
        mActivity9 = new Activity("Activity n.9",
                new ContinuousUniform(mSeedGenerator.generate(), 13, 27));
    }
}
